# frozen_string_literal: true

require 'bundler/setup'
Bundler.setup

require 'simplecov'
SimpleCov.formatter = SimpleCov::Formatter::MultiFormatter.new([
  SimpleCov::Formatter::HTMLFormatter
])
SimpleCov.start { add_filter '/spec/' }

require 'kramdown'
require 'gitlab_kramdown'

# Load support files
Dir[File.join('./spec/support/**/*.rb')].sort.each { |f| require f }

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = '.rspec_status'

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end
